module.exports = function(grunt) {

	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),
		sass: {
		     dist: {
				 files: {
					'css/main.css': 'styles/main.scss'
				}
			}
		},
		ftp_push: {
		    demo: {
		    	options: {
		    		authKey: 'netology',
		    		host: 'university.netology.ru',
		    		dest: '/fbb-store/',
		    		port: 21
		    	},
		    	files: [{
		    		expand: true,
		    		cwd: '.',
		    		src: [
		    		      'index.html',
		    		      'about.html',
		    		      'book.html',
		    		      'order.html',
		    		      'js/*.js',
		    		      'css/main.css',
		    		      'img/*'
		    		]
		        }]
		    }
		},
		watch: {
			css: {
    			files: ['styles/*.scss'],
    			tasks: ['sass'],
    			options: {
        			spawn: false,
    			}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-ftp-push');
	grunt.loadNpmTasks('grunt-contrib-watch');
	
	grunt.registerTask('default', ['sass', 'ftp_push', 'watch']);
	grunt.registerTask('start', ['sass']);

};