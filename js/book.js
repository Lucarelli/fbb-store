//common variables

var bookId = GetURLParameter('bookid'),
    bookCurrency,
    bookCost;

//book load
    
$.get(ref + "book/" + bookId, function( book ) {
    
    $('#book').append($('<div class="book"></div>')
              .append($('<div class="book-cover"></div>')
              .append($('<div id="eye" class="eye"></div>')
              .append($('<div class="eyelid top-eyelid"></div>'))
              .append($('<div class="eyelid bottom-eyelid"></div>'))
              .append($('<div id="eyeCircle" class="eyeCircle"></div>')))
              .append($('<img>').attr("src", book.cover.large)))
              .append($('<p class="book-description"></p>').html(book.description)));
    
//review insert
    
    $('#book').append($('<div id="reviews-block" class="reviews-block"></div>'));
    $.each(book.reviews, function(key, review){
        $('#reviews-block').append($('<div class="review-block"></div>')
                           .append($('<img>').attr("src", review.author.pic))
                           .append($('<p></p>').html(review.cite)));
    });
    
//features insert
    
    $('#book').append($('<div id="features-block" class="features-block"></div>'));
    $.each(book.features, function(key, feature){
        $('#features-block').append($('<div class="feature-block"></div>')
                            .append($('<img>').attr("src", feature.pic))
                            .append($('<p></p>').html(feature.title)));
    });
    
// eye drawing
    
    setTimeout(function(){
        var eyeSize = $('.book-cover img').width()/3,
            eyeLeft = $('.book-cover').width()/2 - eyeSize/2,
            eyeTop = $('.book-cover').height()/2,
            eyeCircleSize = eyeSize * 0.18,
            eyeCirclePosition = eyeSize/2 - eyeCircleSize/1.5;

        $('.eye').css('width', eyeSize)
                 .css('height', eyeSize)
                 .css('left', eyeLeft)
                 .css('top', eyeTop);
        $('.eyeCircle').css('width', eyeCircleSize)
                   .css('height', eyeCircleSize)
                   .css('top', eyeCirclePosition)
                   .css('left', eyeCirclePosition);

        var DrawEye = function(eyeball, eyeCircle, eyeposx, eyeposy){

// eye dep vars

          var r = $(eyeCircle).width()/2;
          var center = {
            x: $(eyeball).width()/2 - r, 
            y: $(eyeball).height()/2 - r
          };
          var distanceThreshold = $(eyeball).width()/2 - r;
          var mouseX = 0, mouseY = 0;

// mouse move

          $(window).mousemove(function(e){

            var d = {
              x: e.pageX - r - eyeposx - center.x,
              y: e.pageY - r - eyeposy - center.y
            };
            var distance = Math.sqrt(d.x*d.x + d.y*d.y);
            if (distance < distanceThreshold) {
              mouseX = e.pageX - eyeposx - r;
              mouseY = e.pageY - eyeposy - r;
            } else {
              mouseX = d.x / distance * distanceThreshold + center.x;
              mouseY = d.y / distance * distanceThreshold + center.y;
            }
          });

// eye circle position

          var eyeCircle = $(eyeCircle);
          var xp = 0, yp = 0;
          var loop = setInterval(function(){
            xp += (mouseX - xp) / 1;
            yp += (mouseY - yp) / 1;
            eyeCircle.css({left:xp, top:yp});    
          }, 1);
        };

        DrawEye("#eye", "#eyeCircle", $('.eye').offset().left, $('.eye').offset().top);
        
        $('.detail-book, .buy-button').animate({opacity: 1}, 1000);
        
    }, 800);
        
//buy button
        
    if( window.innerWidth >= 1366 ){
        $('.buy-button').html('<a href="order.html?bookid=' + book.id + '&currency=' + globalCurrencyId + '">Купить за жалкие ' + book.price + ' Z</a>');  
    }else{
        $('.buy-button').html('<a href="order.html?bookid=' + book.id + '&currency=' + globalCurrencyId + '">Купить<br> за жалкие ' + book.price + ' Z</a>');  
    }
    
    bookCost = book.price;
    bookCurrency = book.currency;
    
    return bookCost, bookCurrency;
        
});