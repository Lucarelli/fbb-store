//vars

var booksNumber = 0,
    allBooksArray;

//books dragging

$(function() {
	
    $('#books').sortable();  

});

//startpage book load

$.get( ref + "book/", function( data ) {
    allBooksArray = data;  
    
    $.each(allBooksArray, function(key, book){
        if(booksNumber < 4){
            bookInsert($('#books'), book, booksNumber);
            booksNumber++;
        }else if( booksNumber == 4){
            allBooksArray.splice(0,4);
            booksNumber++;
        }
    })

});

//add books event

$('#add-books').mousedown(function(){
    $(this).css('transform', 'scale(0.9)');
})

$('#add-books').mouseup(function(){
    $(this).css('transform', 'scale(1)');
})

$('#add-books').click(function(){
    booksNumber = 0;
    $.each(allBooksArray, function(key, book){
        if(booksNumber < 4){
            bookInsert($('#books'), book);
            booksNumber++;
            if(booksNumber == allBooksArray.length){
                $('#add-books').css('display', 'none');
            }
        }else if( booksNumber == 4){
            allBooksArray.splice(0,4);
            booksNumber++;
        }
    })
})