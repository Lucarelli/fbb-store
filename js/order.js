//vars

var bookPrice,
    deliveryPrice = 0,
    deliveryCurrency,
    bookCurrency,
    bookId = GetURLParameter('bookid');

//payment method

function paymentMethod(payment){
    $('.payment-select').append($('<input type="radio" name="payment" class="payment" required>')
                               .attr('id', payment.id)
                               .attr('value', payment.id))
                        .append($('<label for="payment"></label>').html(payment.title))
                        .append($('<br>'));
}

//chosen book info
    
$.get( ref + "book/" + bookId, function( book ) {
    $('.book-name').html('Оформить заказ на книгу <a href="book.html?bookid=' + book.id + '&currency=' + globalCurrencyId + '">' + book.title +'</a>');
    $('.book-cover').attr('src', book.cover.large);
    bookPrice = book.price;
    bookCurrency = book.currency;
    $('.total').html('Итого к оплате: ' + bookPrice + ' USD');
})

//delivery method
         
$.get(ref + "order/delivery", function(delivery) {
    $.each(delivery, function(key, delivery){
        $('.delivery-select').append($('<input type="radio" name="delivery" class="delivery">').attr('id', delivery.id)
                   .attr('data-price', delivery.price)
                   .attr('data-adress', delivery.needAdress)
                   .attr('data-currency', delivery.currency)
                   .attr('value', delivery.id))
                             .append($('<label for="delivery"></label>').html(delivery.name + " - " + delivery.price + " USD"))
                             .append($('<br>'));
    })
    
    //default
        
    $('.delivery-select input:first').attr('checked', true)
                                     .attr('required', true);

    //default checked
    
    $.get(ref + "order/delivery/delivery-01/payment", function(payment) {
        $.each(payment, function(key, payment){
            paymentInsert(payment)
        })
    })
        
    // delivery select

    $(".delivery").click(function(){

        // payments methods reload

        $('.payment-select').html('<p>Способ оплаты:</p>');
        $.get( ref + "order/delivery/" + $(this).attr('id') + "/payment", function( payment ) {
            $.each(payment, function(key, payment){
                paymentMethod(payment)
            })
        })

        // check address necessary

        if($(this).attr('data-adress') == "true"){
            $('.adress').css('display', 'block');
            $('.adress input').attr('required', 'true');
        }else{
            $('.adress').css('display', 'none')
            $('.adress input').attr('required', false);
        }


        // full cost calculating

        deliveryPrice = $(this).attr('data-price');

        total = Number(bookPrice) + Number(deliveryPrice);

        $('.total').html("Итого к оплате: " + total + " " + $('option:selected').attr('data-charcode'));
    })         
})
    
//checkout 
    
$('#buyForm').submit(function(e){
    e.preventDefault();
    var data = {
        'manager' : 'ale.lukashevich@gmail.com',
        'book' : bookId,
        'name' : $(this).find('input[name="name"]').val(),
        'phone' : $(this).find('input[name="phone"]').val(),
        'email' : $(this).find('input[name="email"]').val(),
        'comment' : $(this).find('textarea[name="comment"]').val(),
        'delivery' : {
            'id' : $(this).find('input[name="delivery"]:checked').val(),
            'address' : $(this).find('input[name="adress"]').val()
        },
        'payment' : {
            'id' : $(this).find('input[name="payment"]:checked').val(),
            'currency' : $('option:selected').val(),
        }
    }
        
    $.ajax({
        type: "POST",
        url: ref + "order",
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        beforeSend: function(){
            $('#buyForm').animate({opacity: 0}, 1000);
            
        },
        success: function(result){
            $('.success').css('display', 'block');
        },
        error: function(){
            alert("Пожалуйста, проверьте еще раз введеную вами информацию")
        }
    })
})