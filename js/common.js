//common variables

var ref = "https://netology-fbb-store-api.herokuapp.com/",
    pathNameArr = window.location.pathname.split('/'),
    pathName,
    globalCurrencyId,
    books,
    newCost;

//collect books

$.get( ref + "book/", function( data ) {
    books = data;
    return books;
});

//book insert function

function bookInsert(insertBlock, book){
    insertBlock.append($('<div class="book clearfix"></div>')
               .append($('<div class="book-cover"></div>').html('<a href="book.html?bookid=' + book.id + '&currency=' + globalCurrencyId + '"><img src="' + book.cover.small + '"></a>'))
               .append($('<a class="book-info"></p>').html(book.info).attr('href', 'book.html?bookid=' + book.id + '&currency=' + globalCurrencyId)));
}

//address bar parametrs

function GetURLParameter(param){
    
    var pageURL = window.location.search.substring(1);
    var varsURL = pageURL.split('&');
    for (var i = 0; i < varsURL.length; i++)
    {
        var paramName = varsURL[i].split('=');
        if (paramName[0] == param)
        {
            return paramName[1];
        }
    }
        
}

// bring currency from address bar

globalCurrencyId = GetURLParameter('currency');

if( globalCurrencyId == undefined){
    globalCurrencyId = 'R01235';
}

//page id
    
$.each(pathNameArr, function(key, pathname){
    if (pathname == '' || pathname == 'index.html' || pathname == 'order.html' || pathname == 'book.html' || pathname == 'about.html' ){
        pathName = pathname;
        return pathName;
    }
})

//reference change function

function hrefChange(changeHref, currencyId){
    
    var oldHref = $(changeHref).attr('href'),
        newHref = oldHref.replace(/currency=R\d+\w+/, "currency=" + currencyId);
        $(changeHref).attr('href', newHref);
    
}

//price recalculate

function priceCalc(oldCost, valueVariable, nominalVariable, currToValue, currToNominal){
    
    var currFromValue = $('option[value="' + valueVariable + '"]').attr('data-value'),
        currFromNominal = $('option[value="' + nominalVariable + '"]').attr('data-nominal');
    newPrice = (oldCost * (currFromValue / currFromNominal) / (currToValue / currToNominal)).toFixed(0);
    return newPrice;
    
}

//book price calculating from currency

function href(currencyId){
    
    var currToValue = $('option:selected').attr('data-value'),
        currToNominal = $('option:selected').attr('data-nominal'),
        newCharcode = $('option:selected').attr('data-charcode');
    

    
    if ( pathName == '' || pathName == 'index.html'){
        $.each($('.book-cover a, .book-info'), function(hey, href){
            hrefChange(href, currencyId);
        });
    }
    

    
    if ( pathName == 'book.html' ){
        
        var newBookPrice = priceCalc(bookCost, bookCurrency , bookCurrency , currToValue, currToNominal);
        
        var oldText = $('.buy-button a').text(),
            newText = oldText.replace(/жалкие \w+ \w+/, "жалкие " + newBookPrice + ' ' + newCharcode);
        $('.buy-button a').text(newText);
        
        hrefChange($('.buy-button a'), currencyId);
        
    }
    

    
    if ( pathName == 'order.html' ){
        hrefChange($('.book-name a'), currencyId);
        
        $.each($('.delivery-select input'), function(key, input){
            
            var deliveryOldPrice = $(input).attr('data-price'),
                deliveryOldCurrency = $(input).attr('data-currency');
            
            newDeliveryPrice = priceCalc(deliveryOldPrice, deliveryOldCurrency , deliveryOldCurrency , currToValue, currToNominal)
            
            $(input).attr('data-price', newDeliveryPrice );
            $(input).attr('data-currency', currencyId);
            
            var oldDeliveryText = $(this).next('label').text(),
                newDeliveryText = oldDeliveryText.replace(/- \w+ \w+/, "- " + newDeliveryPrice  + ' ' + newCharcode);
            $(this).next('label').text(newDeliveryText);
            
        });
        
        var newBookPrice = priceCalc(bookPrice, bookCurrency , bookCurrency , currToValue, currToNominal),
            deliveryPrice = $('input[name="delivery"]:checked').attr('data-price'),
            newTotal = Number(newBookPrice) + Number(deliveryPrice),
            oldTotalText = $('.total').text(),
            newTotalText = oldTotalText.replace(/: \w+ \w+/, ": " + newTotal + ' ' + newCharcode);
            $('.total').text(newTotalText);
        
        bookPrice = newBookPrice;
        bookCurrency = $('option:selected').val();
    }
    
    if ( pathName != '/' || pathName != '/index.html'){
        $('header a').attr('href','index.html?currency=' + currencyId);
    }
    if ( pathName != 'about.html'){
        $('footer a').attr('href','about.html?currency=' + currencyId);
    }
}

//currencies list

$.get( ref + "currency/", function(currency) {
    
    $.each(currency, function(key, currency){
        $('#currency').append($('<option data-value="' + currency.Value + '" data-nominal="' + currency.Nominal + '" data-charcode="' + currency.CharCode + '" value="' + currency.ID + '">' + currency.Name + '</option>'));
    });
    
    $('option[value="' + globalCurrencyId + '"]').attr("selected", "selected");
    
    href(globalCurrencyId);
});

// currency change

$('#currency').change(function(){
    
    var currencyId = $(this).val();
        globalCurrencyId = currencyId;
    
    href(currencyId);
    
})


